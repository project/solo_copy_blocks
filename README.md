# Solo Move Blocks Module for Drupal

## Introduction

The Solo Move Blocks module simplifies block and configuration migration from
 the W3CSS theme, or its sub-themes, to the Solo theme in your Drupal
 installation. This module is tailored for W3CSS theme users, ensuring a smooth
 transition.

## Features

- **Easy Migration:** Move blocks from W3CSS theme or sub-themes to Solo with
ease.
- **Block Sorting:** Maintain block order and weight during migration.
- **Theme Compatibility:** Exclusively designed for use with W3CSS theme and
sub-themes.
- **Region Mapping:** Automatically map regions between themes for a seamless
transition.
- **Preserve Block Status:** Retain block status (enabled or disabled) during
migration.

Built for W3CSS theme users, Solo Move Blocks ensures a headache-free migration
 to the new theme.

## Installation

1. **Download and Install the Module:**
   - Download the Solo Move Blocks module from [Drupal.org]
   (https://www.drupal.org/project/solo_move_blocks).
   - Install it on your Drupal website using the standard process.

2. **Install the Solo Theme (Without Setting as Default):**
   - Download the Solo theme from [Drupal.org]
   (https://www.drupal.org/project/solo).
   - During installation, select to install the Solo theme without setting it
   as the default.

3. **Copy Blocks to Solo Theme:**
   - Go to "Configuration" in your Drupal admin panel.
   - Under "System," find "Copy Blocks to Solo Theme."
   - Click it or visit your-site/admin/config/system/solo-copy-blocks.
   - Click "Copy Blocks" to start the block migration process.

## Compatibility

- Compatible with Drupal 8 and Drupal 9.
- Specifically designed for use with the W3CSS theme and its sub-themes.
Compatibility with other themes may vary.


## Maintainers

- [Your Name](https://www.drupal.org/u/flashwebcenter)

## License

This project is licensed under the GNU General Public License, version 2 or
later. See the [LICENSE](./LICENSE.txt) file for details.
